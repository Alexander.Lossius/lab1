//BUG: if human_choice contains several words, then the program loops once for (and with) each word

package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
	
	
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public String randomChoice() {
    	/*
    	 * Chooses random out of rock paper scissors
    	 * :return: random choice
    	 */
    	Random choice = new Random();
    	return(rpsChoices.get(choice.nextInt(rpsChoices.size())));
    }
    
    
    public boolean isWinner(String choice1, String choice2) {
    	/*
    	 * Check if choice1 wins over choice2
    	 * :param choice1
    	 * :param choice2
    	 * :return: true if choice1 beats choice2, false of not
    	 */
    	if (choice1.equals("paper")) {
    		return(choice2.equals("rock"));
    	}
    	else if (choice1.equals("scissors")) {
    		return(choice2.equals("paper"));
    	}
    	else {
    		return(choice2.equals("scissors"));
    	}
    }
    
    
    public String userChoice() {
    	/*
    	 * Prompt the user with what choice of the rock paper scissors they chose
    	 * :return: "rock", "paper", or "scissors"
    	 */
    	while(true) {
    		String human_choice = this.readInput("\nYour choice (Rock/Paper/Scissors)?");
    		
    		if (this.validateInput(human_choice, rpsChoices)) {
    			return(human_choice);
    		}
    		else {
    			System.out.printf("I don't understand %s. Try agian", human_choice);
    		}
    	}
    }
    
    
    public String continuePlaying() {
    	/*
    	 * Prompt the user if they want to continue playing.
    	 * :return: "y" or "n"
    	 */
    	while(true) {
    		String continue_answer = this.readInput("\nDo you wish to continue playing? (y/n)?").toLowerCase();
    		
    		if (this.validateInput(continue_answer, Arrays.asList("y", "n"))) {
    			return(continue_answer);
    		}
    		else {
    			System.out.printf("%nI don't understand %s. Try again", continue_answer);
    		}
    	}
    }
    
    
    
    public boolean validateInput(String input, List<String> valid_inputs) {
        /*
         * Checks if the given input is either rock, paper or scissors.
         * :param input: user input string
         * :param valid_input: array of valid input strings
         * :return: true if valid input, false if not
         */

    	input = input.toLowerCase();
    	return(valid_inputs.contains(input));
    }
    
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

    	while(true) {
    	 
    		System.out.printf("Let's play round %d", roundCounter);
    		
    		// User and Computer choices
    		String human_choice = userChoice();
    		String computer_choice = randomChoice();
    		String choice_string = ("Human chose " + human_choice + ", computer chose " + computer_choice + ".");
    		
    		//Check who wins
    		if (isWinner(human_choice, computer_choice)) {
    			System.out.printf("%s Human wins!", choice_string);
    			++humanScore;
    		} 
    		else if (isWinner(computer_choice, human_choice)) {	
    			System.out.printf("%s Computer wins!", choice_string);
    			++computerScore;
    		}
    		else {
        		System.out.printf("%s Its a tie!", choice_string);
    		} 
    		
    		System.out.printf("%nScore: human %d, computer %d", humanScore, computerScore);
    		
    		// Ask the user if they want to play again
    		String continue_answer = continuePlaying();
    		if (continue_answer.equals("n")) {
    			break;
    		}
    		++roundCounter;
    		
    	}
    	
    	System.out.println("Bye bye :)");
    	
    }
    
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
